namespace nunitproject;

public class TestsOKAndSkip
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void AnotherTestOK()
    {
        Assert.Pass();
    }

    [Test]
    [Ignore("Ignoring this test")]
    public void AnotherTestIgnored()
    {
        Assert.Pass();
    }
}
