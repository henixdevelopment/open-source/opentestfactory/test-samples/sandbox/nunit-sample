namespace nunitproject;

public class Tests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void TestOK()
    {
        Assert.Pass();
    }

    [Test]
    public void TestKO()
    {
        Assert.Fail();
    }

    [Test]
    [Ignore("Ignoring this test")]
    public void TestIgnored()
    {
        Assert.Pass();
    }
}
